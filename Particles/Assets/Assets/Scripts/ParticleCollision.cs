﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollision : MonoBehaviour {

    public Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(Random.Range(-3.0f, 3.0f), 0, Random.Range(-3.0f, 3.0f));
	}
	
	// Update is called once per frame
	void Update () {

        if (rb)
        {
            if (rb.position.y < -20.0f)
            {
                rb.position = new Vector3(Random.Range(-2.0f, 2.0f), 5, Random.Range(-2.0f, 2.0f));
                rb.velocity = new Vector3(Random.Range(-1.0f, 1.0f), 0, Random.Range(-1.0f, 1.0f));
            }
        }
	}

    void OnCollisionEnter(Collision col)
    {
        if (rb)
        {
            if (col.gameObject.tag == "Particle")
            {

                if (col.relativeVelocity.magnitude > 0.1)
                    rb.velocity *= -0.5f;
                else
                    rb.velocity *= 0.1f;
            }
        }
    }
}
