﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ParticleInterface : MonoBehaviour {

    public float m_Attraction;
    public const int size = 10;
    public const int sizecubed = size * size * size;
    public GameObject m_SpawnableObject;
    public float spawnHeight = 5.0f;
    //entry points for DLL
    [StructLayout(LayoutKind.Sequential)]
   public struct Vec
    {
        public float x, y, z;
    }

    [StructLayout(LayoutKind.Sequential)]

    public struct Particle
    {
       public Vec Pos;
       public Vec Vel;
    }
    [DllImport("ParticleSystem")]
    public static extern void initSystem();
    [DllImport("ParticleSystem")]
    public static extern void CleanUpSystem();
    [DllImport("ParticleSystem")]
    public static extern void initialize(Particle[] p, int size);
    [DllImport("ParticleSystem")]
    public static extern void updatePOS(float x, float y, float z);
    [DllImport("ParticleSystem")]
    public static extern void updateAttraction(float f);
    [DllImport("ParticleSystem")]
    public static extern void updatePPOS(Particle[] p, float dt, Vec[] vel, int Size);
    // Use this for initialization
    Particle[] g_Arr = new Particle[sizecubed];
    GameObject[] ParticleArr = new GameObject[sizecubed];
    Rigidbody[] r_Arr = new Rigidbody[sizecubed];
    Vec[] v_Arr = new Vec[sizecubed];

    void SetPosition()
    {
        
        for(int i= 0;i< sizecubed; i++)
        {
            Vec tempV;
            tempV.x = r_Arr[i].velocity.x;
            tempV.y = r_Arr[i].velocity.y;
            tempV.z = r_Arr[i].velocity.z;

            v_Arr[i] = tempV;

            ParticleArr[i].transform.position = new Vector3(g_Arr[i].Pos.x, g_Arr[i].Pos.y, g_Arr[i].Pos.z);
        }
    }
    void Start () {

        for(int i = 0; i< sizecubed; i++)
        {
            ParticleArr[i] = Instantiate<GameObject>(m_SpawnableObject);
            r_Arr[i] = ParticleArr[i].GetComponent<Rigidbody>();
        }
        initSystem();
        updateAttraction(m_Attraction);
        updatePOS(transform.position.x - 2.0f, transform.position.y + spawnHeight, transform.position.z - 1.0f);
        updatePPOS(g_Arr, Time.deltaTime, v_Arr, sizecubed);
        initialize(g_Arr, size);
        SetPosition();
    }
	
	// Update is called once per frame
	void Fixedupdate () {

       
        updatePOS(transform.position.x, transform.position.y, transform.position.z);
        updatePPOS(g_Arr, Time.deltaTime, v_Arr, sizecubed);
        SetPosition();
    }
}
