#pragma once
#include <cmath>
extern "C"
{
	__declspec(dllexport) struct Vec 
	{
		float x, y, z;
	};
	__declspec(dllexport) struct Particle
	{
		Vec Pos;
		Vec Vel;
	};
}

float Length(Vec v);

Vec Normalize(Vec v);



Vec subtract(Vec v, Vec v2);

Vec add(Vec v, Vec v2);

Vec multiply(Vec v, float scalar);


class ParticleSystem
{
public:
	ParticleSystem();
	~ParticleSystem();
	void Update(Particle a_arr[], float dt, Vec vel[], int Size);
	Vec m_Pos;
	Particle *m_ParticleBuff;
};