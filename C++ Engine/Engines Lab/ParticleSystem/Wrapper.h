#pragma once
//#define LIB_API extern "C" __declspec(dllexport) 
#include "ParticleSystem.h"

extern "C"
{
	__declspec(dllexport) void initSystem();
	__declspec(dllexport) void initialize(Particle *p, int Size);
	__declspec(dllexport) void updatePOS(float x, float y, float z);
	__declspec(dllexport) void updatePPOS(Particle *p, float dt, Vec *vel, int Size);
}