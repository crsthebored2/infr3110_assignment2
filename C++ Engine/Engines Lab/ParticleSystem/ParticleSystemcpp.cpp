#include "ParticleSystem.h"
#include <Windows.h>
#include <fstream>

Vec multiply(Vec v, float scalar)
{
	Vec l_v;
	l_v.x = v.x * scalar;
	l_v.y = v.y * scalar;
	l_v.z = v.z * scalar;
	return l_v;
}

float Length(Vec v)
{
	return sqrtf((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
}

Vec Normalize(Vec v)
{
	Vec l;
	float l_Length = Length(v);
	if (l_Length != 0)
	{
		l.x = v.x / l_Length;
		l.y = v.y / l_Length;
		l.z = v.z / l_Length;
	}
	return l;
}



Vec subtract(Vec v, Vec v2)
{
	Vec l_v;
	l_v.x = v.x - v2.x;
	l_v.y = v.y - v2.y;
	l_v.z = v.z - v2.z;

	return l_v;
}
Vec add(Vec v, Vec v2)
{
	Vec l_v;
	l_v.x = v.x + v2.x;
	l_v.y = v.y + v2.y;
	l_v.z = v.z + v2.z;
	return l_v;
}
ParticleSystem::ParticleSystem() 
{
}

ParticleSystem::~ParticleSystem() 
{
	delete[] m_ParticleBuff;
}

void ParticleSystem::Update(Particle a_arr[], float dt, Vec vel[], int Size) 
{

	for (unsigned int i = 0; i < Size; i++)
	{
		a_arr[i].Vel = multiply(vel[i], dt);
		a_arr[i].Pos = add(a_arr[i].Pos, a_arr[i].Vel);
	}
}
