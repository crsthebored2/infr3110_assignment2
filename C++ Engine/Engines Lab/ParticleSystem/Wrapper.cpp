#include "Wrapper.h"
#include "ParticleSystem.h"
#include <windows.h>
#include <fstream>
#include <ctime>

ParticleSystem *g_PS;

void initSystem() 
{
	g_PS = new ParticleSystem();
	srand((unsigned)time(0));
}

void initialize(Particle *p, int Size)
{
	int countX = 0;
	int countY = 0;
	int countZ = 0; 
	for (int i = 0; i < 1000; i++)
	{
		p[i].Pos.x = (g_PS->m_Pos.x + countX) / 8;
		p[i].Pos.y = (g_PS->m_Pos.y + countY) / 8;
		p[i].Pos.z = (g_PS->m_Pos.z + countZ) / 8;
		p[i].Vel.x = 0.0f;
		p[i].Vel.y = 0.0f;
		p[i].Vel.z = 0.0f;
		countX++;
	
		if (countX % 10 == 0)
		{
			countZ++;
			countX = 0;
		}
		if (countZ % 10 == 0 && countX % 10 == 0)
		{
			countY++;
			countZ = 0;
		}
	}
}

void updatePOS(float x,float y, float z)
{
	g_PS->m_Pos.x = x;
	g_PS->m_Pos.y = y;
	g_PS->m_Pos.z = z;
}

void updatePPOS(Particle *p, float dt, Vec *vel, int Size)
{
	g_PS->Update(p, dt, vel, Size);
}